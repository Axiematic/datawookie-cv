---
layout: single
title: Andrew B. Collier
---

<h2 class="resume">Experience</h2>

<p>More details of these positions can be found on my <a href="http://www.linkedin.com/in/datawookie">LinkedIn profile</a>.</p>

- [AWS Founders Tier](https://aws.amazon.com/activate/)

<h3>Training</h3>

Certifications:

- Software/Data Carpentry Instructor
- [RStudio Instructor](https://education.rstudio.com/trainers/people/collier+andrew/)

{{% notprint %}}
A selection of training courses I have given recently:

- R for Social Scientists (Data Carpentry), CODATA-RDA Research Data Summer School, University of Pretoria, January 2020.
- Introduction to Deep Learning, The Wanderers Club, Johannesburg, October 2019.
- Machine Learning with R, The Sett, Durban, September 2019.
- [SQL Server 2019 Big Data Cluster](https://www.sqlsaturday.com/897/eventhome.aspx), SQL Saturday, Cape Town, September 2019.
- [SQL Server 2019 Big Data Cluster](https://www.sqlsaturday.com/903/eventhome.aspx), SQL Saturday, Johannesburg, September 2019.
- Introduction to Data Science with R, Bandwidth Barn, Cape Town, June 2019.
- Version control with Git, [UKZN Software Carpentry Workshop](https://workshops-ukzn.github.io/2018-12-04-UKZN-SWC) (Durban), December 2018.
- Introduction to SQL, [Democratic Alliance](https://www.da.org.za/) (Cape Town), October 2018.
- Machine Learning with Python, [PyCon](https://za.pycon.org/) (Johannesburg), October 2018.
- Introduction to Spark, [Vantage Data](https://www.vantagedata.co.za/), September 2018.
- Automated Reporting with R: Essentials, [Thermo Fisher](https://www.brahms.de/) (Hennigsdorf, Germany), July 2018.
- Building Packages with R: Essentials, [Thermo Fisher](https://www.brahms.de/) (Hennigsdorf, Germany), July 2018.
- Data Science with R: Beginners' Course, Investec (Johannesburg), June 2018.
- Web Scraping with R, Rise (Cape Town), June 2018.
- Web Scraping with Python, [PyCon](https://za.pycon.org/) (Cape Town), October 2017.
- Data Science (Head Teacher) , <a href="http://www.ixperience.co.za">iXperience</a>, 2016, 2017 and 2018.

  <li>A. B. Collier, and K. Tirok, "Introduction to R." (1 day workshop) Department of Public Health, University of KwaZulu-Natal, Durban, South Africa, 2017.</li>

I created and maintain the [Machine Learning with Apache Spark](https://www.datacamp.com/courses/machine-learning-with-apache-spark) course on DataCamp.

Conferences I have organised: satRday *Johannesburg* (<a href="https://joburg2020.satrdays.org/">2020</a> and <a href="https://joburg2019.satrdays.org/">2019</a>) and satRday *Cape Town* (<a href="http://capetown2018.satrdays.org/">2018</a> and <a href="http://capetown2017.satrdays.org/">2017</a>).
{{% /notprint %}}

<h2 class="resume">Education</h2>

<table class="education">
<tr><td>PhD</td><td>Space Physics</td><td>Royal Institute of Technology, Stockholm</td> <td class="duration">2006</td></tr>
<tr><td></td><td colspan="3"><em>Thesis: <a href="https://www.researchgate.net/publication/235672270_VLF_and_ULF_Waves_Associated_with_Magnetospheric_Substorms">VLF and ULF Waves Associated with Magnetospheric Substorms</a></em></td></tr>
<tr><td>Licentiate</td><td>Space Physics</td><td>Royal Institute of Technology, Stockholm</td> <td class="duration">2004</td></tr>
<tr><td></td><td colspan="3"><em>Thesis: <a href="https://www.researchgate.net/publication/304743709_VLF_Chorus_Activity_Associated_with_Substorm_Particle_Injections">VLF Chorus Activity Associated with Substorm Particle Injections</a></em></td></tr>
<tr><td>MSc</td><td>Physics</td><td>Potchefstroom University</td><td class="duration">1998</td></tr>
<tr><td></td><td colspan="3"><em>Thesis: Application of a Hybrid Monte Carlo Method to Neutral Particle Transport in Slab Geometry</em></td></tr>
<tr><td>BSc (Honours)</td><td>Physics</td><td>University of Natal</td><td class="duration">1993</td></tr>
<tr><td>BSc</td><td>Physics & Mathematics</td><td>University of Natal</td><td class="duration">1992</td></tr>
</table>

<!--
Students supervised:

- Brett Delport (MSc)
- Etienne Koen (MSc)
- Marlie van Zyl
- Stephen Meyer (MSc)
- Sherry Bremner (MSc)
- Brett Delport (PhD)
- Etienne Koen (PhD)
-->

<h2 class="resume">Technical Skills</h2>

<!--
<img src="https://www.dropbox.com/s/hx0zd8q0wzj7nrq/skills-matrix.svg?raw=1"></img>
-->

### Fluent

<ul> <!-- Packages on CRAN: <a href="https://cran.r-project.org/web/packages/feedeR/index.html">feedeR</a>, <a href="https://cran.r-project.org/web/packages/liqueueR/index.html">liqueueR</a> and <a href="https://cran.r-project.org/web/packages/ubeR/index.html">ubeR</a>. -->
</ul>

### Competent

<ul>
  <li> Shiny
  <li> Spark
  <li> Stan (with PyStan and RStan)
  <li> Django, Hugo, Jekyll, Bootstrap, HTML, CSS and SASS
  <!-- <li> JavaScript and D3 -->
  <!-- <li> JSON and XML -->
</ul>

### Conversant

<ul>
  <li> Julia, C, C++ and FORTRAN
  <li> Tableau
  <li> GIS
</ul>

{{< comment >}}
{{% notprint %}}
<h2 class="resume">Certifications</h2>

<h3>Technical</h3>

<ul>
  <li><a href="https://software-carpentry.org/">Software Carpentry</a> Instructor</li>
  <li><a href="https://community.rstudio.com/groups/consultants/">R/RStudio Consultant</a></li>
  <li>AWS: <a href="https://www.aws.training/transcript/CompletionCertificateHtml?transcriptid=HV17BMLBU0aYB_P8DYxYtg2">AWS Technical Professional</a></li>
</ul>

Introduction to Apache Spark and AWS;
Data Science Specialisation <!-- Johns Hopkins University  --> (The Data Scientist’s Toolbox; R Programming; Getting and Cleaning Data; Exploratory Data Analysis; Reproducible Research; Statistical Inference; Regression Models; Developing Data Products; Practical Machine Learning);
Data Analysis; <!-- Johns Hopkins University  -->
Machine Learning;  <!-- Stanford Online  -->
Statistical Learning;  <!-- Stanford Online  -->
Computing for Data Analysis;  <!-- Johns Hopkins Bloomberg School of Public Health  -->
Intro to Python for Data Science;  <!-- DataCamp  -->
Python: Beyond the Basics;  <!-- Pluralsight  -->
Understanding NoSQL;  <!-- Pluralsight  -->
Writing Content With Markdown;  <!-- Pluralsight  -->
Bootstrap 3;  <!-- Pluralsight  -->
D3.js Essential Training for Data Scientists; <!-- Lynda -->
JavaScript from Scratch;  <!-- Pluralsight  -->
Introduction to Presentation Design;  <!-- Pluralsight  -->
Git Fundamentals;  <!-- Pluralsight  -->
LinkedIn Fundamentals;  <!-- Pluralsight  -->
Twitter for Business;  <!-- Pluralsight  -->
Heterogeneous Parallel Programming;  <!-- University of Illinois at Urbana-Champaign  -->
Principles of Economics for Scientists;  <!-- California Institute of Technology  -->
Crafting Quality Code;  <!-- University of Toronto  -->
Introduction to C# Programming;  <!-- Microsoft  -->
Computational Photography;  <!-- Georgia Institute of Technology  -->
Computational Investing.

<h3>Soft Skills</h3>

<p>Certified ScrumMaster.</p>

Creating and Leading Effective Teams for Managers; <!-- Pluralsight  -->
How to be a Great Mentor; <!-- DERIVCO  -->
Working on a Team; <!-- Pluralsight  -->
Leadership: Getting Started; <!-- Pluralsight  -->
<!-- <li> Resumes and Self-marketing for Software Developers <!-- Pluralsight  -->
Project Management Programme; <!-- Peakford Management Consultants  -->
Toastmasters International Speechcraft Course; <!-- Toastmasters  -->
Derivco Leadership Code;
How to Start and Run a Successful Freelancing Business. <!-- Pluralsight  -->
{{% /notprint %}}
{{< /comment >}}

{{% notprint %}}
<h2 class="resume">Interests</h2>

My interests include: R, data, web development, running (road and trail), photography, gardening, food (especially garlic, capers and red wine).
{{% /notprint %}}

<div class="onlyprint">
  A more detailed version of this document can be found at <a href="/">{{< ref "/" >}}</a>.
</div>
